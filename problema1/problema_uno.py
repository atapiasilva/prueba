# encoding: utf-8
import pandas as pd

df = pd.read_json('data1.json', orient='columns')

var1_global = []
var2_pronvicia2 = []
var_1_region4 = []

for v in df.iterrows():
    region = v[1][1]    # nodo region
    for i in v[1][0]:   # nodo provincia
        for k in i['children']: # nodo ciudad
            var1_global.append(k['values']['var1'])   # valores var1
            if 'Provincia2' == i['name']:
                var2_pronvicia2.append(k['values']['var2'])
            if region == 'Region4':
                var_1_region4.append(k['values']['var1'])

# Extraer el promedio de la variable var1
print("Promedio var1 :",sum(var1_global)/len(var1_global))

# Extraer la suma de la variable var2 para la provincia 2
print("Sumatoria de Provincia2 :",sum(var2_pronvicia2))

# Extraer el máximo de la variable var1 de la región 4
print("Máximo de var1 de la Región 4 :",max(var_1_region4))
